# pylint: disable=invalid-name

from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable, Iterable
from IPython.display import Latex


@dataclass
class ComposableLatex:
    expression: str

    def __add__(self, other: ComposableLatex) -> ComposableLatex:
        if other in (0, None):
            return self
        return ComposableLatex(expression=f"{self} {other}")

    def __radd__(self, other: Any) -> ComposableLatex:
        if other in (0, None):
            return self
        return ComposableLatex(expression="f{other} {self}")

    def join(self, others: Iterable[Any]) -> ComposableLatex:
        if not others:
            return None
        i = iter(others)
        result = next(i)
        for current in i:
            result = result + self + current
        return result

    def eq(self, other: Any) -> ComposableLatex:
        return ComposableLatex(f"{self} &= {other}")

    def __rshift__(self, function: Callable[[ComposableLatex], Any]) -> Any:
        return function(self)

    def __rlshift__(self, function: Callable[[ComposableLatex], Any]) -> Any:
        return function(self)

    def __str__(self):
        return self.expression


def text(value: str) -> ComposableLatex:
    return ComposableLatex(f"\\text{{{value}}}")


def P(value: Any) -> ComposableLatex:
    return ComposableLatex(f"P({value})")


def frac(numerator: Any, denominator: Any) -> ComposableLatex:
    return ComposableLatex(f"\\frac{{{numerator}}}{{{denominator}}}")


def align(expression: Any) -> ComposableLatex:
    return ComposableLatex(f"\\begin{{align}} {expression} \\end{{align}}")


def render(expression: Any) -> Latex:
    return Latex(f"$$ {expression} $$")


wedge = land = ComposableLatex(r"\wedge")
newline = ComposableLatex("\\\\")
space = ComposableLatex("\\;")
