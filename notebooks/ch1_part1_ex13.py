import numpy as np

def many_men(
        count: int,
        iterations: int = 365,
        threshold: float = .6
) -> int:
    girl_boy = [0, 1]
    births = np.random.choice(
        girl_boy, size=(iterations, count)
    )
    boys = births.sum(1)
    return (boys >= (threshold * count)).sum() or 0

print(f"Hospital with 45 births/day: {many_men(45)}")
print(f"Hospital with 15 births/day: {many_men(15)}")

