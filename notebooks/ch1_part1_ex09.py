import pandas as pd
from random import choice

wheel = ['00'] + [str(v) for v in range(37)]
reds = {str(v) for v in range(1, 19)}
blacks = {str(v) for v in range(19, 37)}

assert len(reds) == len(blacks)

def labouchere() -> pd.DataFrame:
    counts = [1, 2, 3, 4]
    bets = [0]
    sizes = [sum(counts)]

    while counts:
        slot = choice(wheel)
        bet = (
            counts[0] + counts[-1]
            if len(counts) > 1 else
            counts[0]
        )
        if slot in reds:
            if len(counts) > 2:
                counts = counts[1:-1]
            else:
                counts = []
        else:
            counts.append(bet)
        bets.append(bet)
        sizes.append(sum(counts))

    return pd.DataFrame({'bet': bets, 'sizes': sizes})
