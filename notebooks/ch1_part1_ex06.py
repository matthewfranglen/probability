import pandas as pd
import numpy as np

wheel = ['00'] + [str(v) for v in range(37)]
reds = {str(v) for v in range(1, 19)}
blacks = {str(v) for v in range(19, 37)}

assert len(reds) == len(blacks)

spins = pd.DataFrame({'slot':
    np.random.choice(wheel, size=1000)
})
wins = len(spins[spins['slot'].isin(reds)])

print(wins * 2)
