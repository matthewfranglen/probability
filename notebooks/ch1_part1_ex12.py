from typing import Tuple
import numpy as np
import pandas as pd

def vote(split: float, voters: int) -> Tuple[int, int]:
    votes = np.random.random(voters)
    return (
        (votes < split).sum() or 0,
        (votes >= split).sum() or 0,
    )

def accuracy(
        iterations: int, split: float, voters: int
) -> pd.DataFrame:
    expected = split > .5
    mid = voters / 2
    df = pd.DataFrame({
        'votes': [
            (vote(split, voters)[0] > mid) == expected
            for _ in range(iterations)
    ]})
    return len(df[df.votes]) / len(df)
