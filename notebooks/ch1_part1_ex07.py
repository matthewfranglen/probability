import pandas as pd
import numpy as np
from functools import partial

wheel = ['00'] + [str(v) for v in range(37)]
reds = {str(v) for v in range(1, 19)}
blacks = {str(v) for v in range(19, 37)}

assert len(reds) == len(blacks)

def calculate(rounds: int = 1000) -> pd.DataFrame:
    df = pd.DataFrame({'slot':
        np.random.choice(wheel, size=rounds)
    })
    df['is red'] = df['slot'].isin(reds)
    df['is 17'] = df['slot'] == '17'

    def winnings(value: bool, win: int = 1) -> int:
        return win if value else -1

    df['red gains'] = (
        df['is red']
            .apply(winnings)
            .cumsum()
    )
    df['17 gains'] = (
        df['is 17']
            .apply(partial(winnings, win=35))
            .cumsum()
    )
    return df
