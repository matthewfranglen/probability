import pandas as pd
from random import random

def raquetball():
    limit = 21
    serve = 0
    win = [0.6, 0.5]
    score = [0, 0]

    while max(score) < limit:
        serve = 0 if random() < win[serve] else 1
        score[serve] += 1
    return score[0] >= limit

count = 100_000
df = pd.DataFrame({'win': [raquetball() for _ in range(count)]})
print(len(df[df['win']]) / len(df))
