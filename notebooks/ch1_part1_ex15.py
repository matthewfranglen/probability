
import pandas as pd
from random import choice

def has_streak() -> bool:
    is_hit = [False, True]
    count = 0

    for _ in range(20):
        if choice(is_hit):
            count += 1
            if count >= 5:
                return True
        else:
            count = 0
    return False

df = pd.DataFrame({'streak': [
    has_streak()
    for _ in range(10_000)
]})
chance = df.streak.mean() * 100
print(f"5 hit streaks occur in {chance}% of games")
