import pandas as pd
import numpy as np

def coin_toss(
        iterations: int, rounds: int = 40
) -> pd.DataFrame:
    choices = [-1, 1]
    plays = np.random.choice(
        choices, size=(iterations, rounds)
    )
    df = pd.DataFrame({'winnings': plays.sum(1)})
    df['winnings'] = df['winnings'].sort_values()
    return df
