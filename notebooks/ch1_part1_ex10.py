from typing import Tuple
import pandas as pd
from random import choice

wheel = ['00'] + [str(v) for v in range(37)]
reds = {str(v) for v in range(1, 19)}
blacks = {str(v) for v in range(19, 37)}

assert len(reds) == len(blacks)

def martingale(
        win: int = 5, limit: int = 100
) -> pd.DataFrame:
    bet = 1
    gain = 0
    bets = []
    gains = []

    while gain < win and gain > -limit:
        bets.append(bet)
        g, bet = spin(bet)
        gain += g
        gains.append(gain)

    return pd.DataFrame({'bet': bets, 'gains': gains})

def min_ngale(
        iterations: int, rounds: int
) -> pd.DataFrame:
    return pd.DataFrame({
        'min gain': sorted([
            min(ngale(rounds).gains)
            for _ in range(iterations)
    ])})

def ngale(rounds: int) -> pd.DataFrame:
    bet = 1
    gain = 0
    bets = []
    gains = []

    for _ in range(rounds):
        bets.append(bet)
        g, bet = spin(bet)
        gain += g
        gains.append(gain)

    return pd.DataFrame({'bet': bets, 'gains': gains})

def spin(bet: int) -> Tuple[int, int]:
    slot = choice(wheel)
    if slot in reds:
        return (bet, 1)
    return (-bet, bet * 2)
