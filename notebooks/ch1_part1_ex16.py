import pandas as pd
from random import choice

def first_boy() -> int:
    is_boy = [False, True]
    count = 1

    while not choice(is_boy):
        count += 1
    return count

def both():
    gender = ['girl', 'boy']
    children = set()
    count = 0

    while len(children) < 2:
        children.add(choice(gender))
        count += 1
    return count

length = 10_000
df = pd.DataFrame({
    'boy': [
        first_boy()
        for _ in range(length)
    ],
    'both': [
        both()
        for _ in range(length)
]})
print(f"First boy children: {df.boy.mean()}")
print(f"Both children: {df.both.mean()}")
