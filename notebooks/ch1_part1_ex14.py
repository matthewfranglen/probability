import pandas as pd
from random import choice

def toss() -> int:
    is_head = [False, True]
    count = 1
    while not choice(is_head):
        count += 1
    return 2**count

df = pd.DataFrame({
    'run': [
        toss()
        for _ in range(10_000)
]})
print(df.head())
print(f"Average run length: {df.run.mean()}")
