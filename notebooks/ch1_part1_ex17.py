import pandas as pd
from itertools import permutations
from math import inf
from random import choice
from typing import Callable, List, Tuple

DIRECTIONS = [-1, 1]

def return_1d(limit: int = 100_000) -> float:
    offset = choice(DIRECTIONS)
    steps = 1.

    while offset != 0:
        offset += choice(DIRECTIONS)
        steps += 1
        if steps > limit:
            return inf
    return steps

def return_2d(limit: int = 1000) -> float:
    directions = offsets(2)
    offset = choice(directions)
    steps = 1.

    while not at_origin(offset):
        offset = step(offset, directions)
        steps += 1
        if steps > limit:
            return inf
    return steps

def step(
        position: List[int],
        directions: List[List[int]]
) -> List[int]:
    offset = choice(directions)
    return list(map(sum, zip(position, offset)))

def at_origin(position: List[int]) -> bool:
    return all(v == 0 for v in position)

def offsets(dimensions: int) -> List[List[int]]:
    assert dimensions > 0
    if dimensions == 1:
        return map(lambda v: [v], DIRECTIONS)
    return [
        offset + [direction]
        for offset in offsets(dimensions - 1)
        for direction in DIRECTIONS
    ]

def sample(
        f: Callable[[], int],
        iterations: int
) -> Tuple[int, pd.DataFrame]:
    df = pd.DataFrame({
        'steps': sorted([
            f()
            for _ in range(iterations)
    ])})
    never = len(df[df['steps'] == inf])
    df = df[df['steps'] < inf].copy()
    return (never, df)
